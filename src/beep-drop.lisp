(in-package #:cl-user)
(defpackage #:beep-drop
  (:use #:cl #:sdl2 #:board))
(in-package #:beep-drop)

;;;--------------CONSTANTS--------------
(defconstant +outer-width+ 30)
(defconstant +inner-width+ 20)

;;;--------------VARIABLES--------------
(defparameter *colors* (make-array '(5) :initial-contents
                             '(((28 86 28) (28 100 28)) 
                               ((86 28 28) (100 28 28)) 
;                               ((28 28 86) (28 28 100)) 
                               ((86 28 86) (100 28 100))
                               ((28 86 86) (28 100 100))
                               ((86 86 28) (100 100 28)))))

;;; Change this to a let somewhere later.
(defvar *board* (make-instance 'board))
;;; Change this to a let somewhere later.
(defvar *player-column* 2)
;;; Change this to a let somewhere later.
(defvar *player-stack* nil)
;;; Change this to a let somewhere later.
(defvar *new-row-delta* 0)

;;;--------------FUNCTIONS--------------
(defun generate-row ()
  "Generates a row of random colors."
  (loop for i from 1 to board:+width+
        collect (aref *colors* (random (length *colors*)))))

(defun clear (renderer)
  "Clears the screen."
  (sdl2:set-render-draw-color renderer 0 0 0 255)
  (sdl2:render-clear renderer))

(defun draw-brick (renderer color position)
  "Draws a brick.
COLOR is a list of two elements--one for the outer color, 
                                 and one for the inner color.
POSITION is a list of an x and y coordinate."
  (flet ((set-brick-color (color)
            (let ((r (first color))
                  (g (second color))
                  (b (third color)))
              (sdl2:set-render-draw-color renderer r g b 255))))

    (let ((x (first position))
          (y (second position)))
      (set-brick-color (first color))
      (sdl2:render-fill-rect renderer (sdl2:make-rect x y +outer-width+ +outer-width+))
      (set-brick-color (second color))
      (sdl2:render-fill-rect renderer (sdl2:make-rect (+ x 5) (+ y 5) +inner-width+ +inner-width+)))))

(defun draw-board (renderer board)
  "Draws the board to the screen."
  (let ((columns (columns board)))
    (loop for x from 0 to (1- board:+width+)
          ;; loop going top to bottom from left to right.
          do (loop for y from 0 to (1- board:+height+)
                   do (let ((color (aref columns x y))
                            (position `(,(* x +outer-width+) ,(* y +outer-width+))))
                        ;; Only draw when a color exists.
                        (when color
                          (draw-brick renderer color position)))))))

(defun draw-player (renderer column)
  (let ((color '((150 150 150) (180 180 180)))
        (position `(,(* column +outer-width+) ,(* (+ board:+height+ 2) +outer-width+))))
    (draw-brick renderer color position)))

(defun render-state (renderer board player-column)
  "Renders the current state."
  (clear renderer)
  (draw-board renderer board)
  (draw-player renderer player-column)
  (sdl2:render-present renderer))

;;; First, we need to create boards.
;;; Then we need to spawn a few rows of bricks to start with.
;;; Every x amount of time, a new row is added.
(defun main ()
  (setf *board* (make-instance 'board))
  (setf *player-column* 2)
  (setf *player-stack* nil)
  (sdl2:with-init (:everything)
    (format t "Using SDL Library Version: ~D.~D.~D~%"
            sdl2-ffi:+sdl-major-version+
            sdl2-ffi:+sdl-minor-version+
            sdl2-ffi:+sdl-patchlevel+)
    (finish-output)
    (sdl2:with-window (win :title "Beep Drop" :flags '(:shown))
      (sdl2:with-renderer (renderer win)
        (sdl2:with-event-loop (:method :poll)
          (:keydown (:keysym keysym)
            (let ((scancode (sdl2:scancode-value keysym)))
              (cond
                ((sdl2:scancode= scancode :scancode-q) (sdl2:push-event :quit))
                ((sdl2:scancode= scancode :scancode-space) (if *player-stack*
                                                             ;; Add the column
                                                             (progn 
                                                               (add-column *board* *player-column* *player-stack*)
                                                               (loop with position
                                                                     while (setf position (find-run-column *board* *player-column*))
                                                                     do (remove-run *board* position))
                                                               (setf *player-stack* nil))
                                                             
                                                             (setf *player-stack* (grab-column *board* *player-column*))))
                ((sdl2:scancode= scancode :scancode-left) (unless (= *player-column* 0)
                                                            (decf *player-column*)
                                                            (render-state renderer *board* *player-column*)))
                ((sdl2:scancode= scancode :scancode-right) (unless (= *player-column* 5)
                                                             (incf *player-column*)
                                                             (render-state renderer *board* *player-column*))))))
          (:idle () 
            (clear renderer)
            (when (when (= *new-row-delta* 60)
                    (setf *new-row-delta* 0)
                    (add-row *board* (generate-row)))
              (sdl2:push-event :quit))
            (render-state renderer *board* *player-column*)
            
            (incf *new-row-delta*)
            (sleep .06))
          (:quit () t))))))
