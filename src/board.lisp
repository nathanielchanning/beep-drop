(in-package #:cl-user)
(defpackage #:board
  (:use #:cl)
  (:export
   ;; Constants
   +height+
   +width+
   +possible-directions+

   ;; Classes
   board

   ;; Accessors
   current-position
   columns

   ;; Methods
   get-position
   add-row
   columns
   grab-column
   add-column; need this still
   find-run-direction
   find-run
   find-run-column
   remove-run))
(in-package #:board)

;;;--------------CONSTANTS--------------
(defconstant +height+ 15
  "The number of positions going down on the board.")

(defconstant +width+  6
  "The number of positions across on the board.")

(defconstant +run-length+ 4
  "The minimum number of the same color in a row to form a removeable run.")

(defclass board ()
  ((columns          :accessor columns
                     :initform (make-array `(,+width+ ,+height+)
                                           :initial-element nil
                                           :adjustable t)
                     :initarg :columns
                     :documentation "The columns of the board.")

   ;; Start with -1 for 0 based indexing.
   (current-position :accessor current-position
                     :initform (make-array +width+
                                           :initial-element -1)
                     :documentation "The current number of 
elements - 1 in their respective columns.")))

;;;--------------GENERICS---------------
(defgeneric get-position (board position)
  (:documentation "Gets POSITION from the columns in BOARD."))

(defgeneric set-position (board position item)
  (:documentation "Sets POSITION from the columns in BOARD to ITEM."))

(defgeneric shift-column (board column-num new-item)
  (:documentation "Shifts the column down, adding in the new item.
Returns NIL if the column can't be shifted anymore. Otherwise it
returns BOARD."))

(defgeneric add-row (board row)
  (:documentation "Adds ROW to the bottom of the BOARD. 
Expects ROW to be length +WIDTH+. Returns NIL if the row
was added, otherwise it returns T if it can't be added."))

(defgeneric grab-column (board column-num)
  (:documentation "Pops the top color off of a column 
and continues grabbing while the color is the same.
Note that even if it is nil, a list will be returned."))

(defgeneric add-column (board column-num column)
  (:documentation "Pushes a list to the front of a column.
Returns all items that weren't added."))

(defgeneric find-run-direction (board position direction)
  (:documentation "Checks if there is a run in DIRECTION."))

(defgeneric find-run (board position)
  (:documentation "Checks every direction from POSITION for a run
of the same color."))

(defgeneric find-run-column (board column-num)
  (:documentation "Checks COLUMN for a run, and if found, returns the
first run."))

(defgeneric condense-column (board column-num)
  (:documentation "Condenses the column towards the bottom by 
replacing nils with any colors in front of them."))

(defgeneric remove-run (board position)
  (:documentation "Removes the run at position from the board."))

;;;-----------HELPER-METHODS------------
(defmethod get-position ((board board) (position list))
  (aref (columns board) (first position) (second position)))

(defmethod set-position ((board board) (position list) new-item)
  (setf (aref (columns board) (first position) (second position)) new-item)
  board)

(defmethod shift-column ((board board) column-num new-item)
  (unless (get-position board `(,column-num ,(- +height+ 1)))
    (loop for height from (- +height+ 1) downto 1
          do (let ((position-1 `(,column-num ,height))
                   (position-2 `(,column-num ,(- height 1))))
               (set-position board position-1 (get-position board position-2)))
          finally (set-position board `(,column-num 0) new-item))))

(defmethod condense-column ((board board) column-num)
  (flet ((slide-position (position)
           (let ((next-position (unless (= (second position) (1- +height+))
                                  `(,(first position) ,(1+ (second position))))))
             (when next-position
               (prog1
                 t 
                 (let ((next-color (get-position board next-position)))
                   (set-position board position next-color)
                   (set-position board next-position nil)))))))
    (loop for i = 0 then (if (get-position board `(,column-num ,i)) (1+ i) i)
          while (not (= (1- i) (aref (current-position board) column-num)))
          unless (get-position board `(,column-num ,i))
          do (loop for j from i to (1- (aref (current-position board) column-num))
                   do (slide-position `(,column-num ,j))
                   finally (decf (aref (current-position board) column-num))))))

;;;--------------METHODS----------------
(defmethod add-row ((board board) (row list))
  ;; Check if a row can be added.
  (if (loop for element across (current-position board)
            when (>= element (1- +height+))
              return t)
    ;; If it can't, indicate that it can't be added.
    t
    (loop for item in row
          for column-num from 0 to (- +height+ 1)
          do (progn
               (when item
                 (incf (aref (current-position board) column-num)))
               (shift-column board column-num item)))))

(defmethod grab-column ((board board) column-num)
  (unless (>= -1 (aref (current-position board) column-num))
    (let* ((current-row (aref (current-position board) column-num))
           (current-position `(,column-num ,current-row))
           (color (get-position board current-position)))
      ;; This funtion grabs the same color if possible,
      ;; and sets the corresponding position to nil.
      (flet ((grab-same-color (position)
                              (when (equal (get-position board position) color)
                                (set-position board position nil))))
        (loop while (and (<= 0 (second current-position)) 
                         (grab-same-color current-position))
              ;; Collect the color.
              collect color into list-of-color 
              ;; Decrement the row we're at each time.
              do (progn
                   (setf current-position `(,column-num ,(1- (second current-position))))
                   (decf (aref (current-position board) column-num)))
              finally (return list-of-color))))))

(defmethod add-column ((board board) column-num (column list))
  (loop for i from (1+ (aref (current-position board) column-num)) to (1- +height+)
        while column
        do (let ((position `(,column-num ,i)))
             (set-position board position (pop column))
             (incf (aref (current-position board) column-num)))
        finally (return column)))

(defmethod find-run-direction ((board board) position direction)
  (let* ((color (get-position board position))
         (next-positions  (loop for x from 1 to 3
                                collect (list (+ (first position) (* (first direction) x)) 
                                              (+ (second position) (* (second direction) x)))))
         (opposite-next-positions (loop for x from 1 to 3
                                        collect (list (+ (first position) (* (first direction) x -1))
                                                      (+ (second position) (* (second direction) x -1)))))
         (valid-positions (loop for pos in next-positions
                                ;; Check the bounds.
                                when (and (<= 0 (first pos))
                                          (<= 0 (second pos))
                                          (< (first pos) +width+)
                                          (< (second pos) +height+))
                                collect pos))
         (valid-opposite-positions (loop for pos in opposite-next-positions
                                         ;; Check the bounds.
                                         when (and (<= 0 (first pos))
                                                   (<= 0 (second pos))
                                                   (< (first pos) +width+)
                                                   (< (second pos) +height+))
                                         collect pos))
         (colors                (mapcar #'(lambda (pos) (get-position board pos)) valid-positions))
         (opposite-colors       (mapcar #'(lambda (pos) (get-position board pos)) valid-opposite-positions))
         (valid-colors          (mapcar #'(lambda (current-color) (equal color current-color)) colors))
         (valid-opposite-colors (reverse (mapcar #'(lambda (current-color) (equal color current-color)) opposite-colors)))
         ;; Now we have a list of t and nil.
         ;; pop off the front until we run out of t's.
         (run-count (loop for length = 1 then (1+ length)
                          while (pop valid-colors)
                          finally (return length)))
         (opposite-run-count (loop for length = 0 then (1+ length)
                                   while (pop valid-opposite-colors)
                                   finally (return length))))
    (+ run-count opposite-run-count)))

(defmethod find-run ((board board) position)
  (let* ((possible-directions '((1 0) (0 1)))
         (runs (mapcar #'(lambda (direction) 
                           (find-run-direction board position direction)) 
                       possible-directions))
         (removeable-runs (remove-if-not #'(lambda (run) (<= 4 run)) runs)))
    removeable-runs))

(defmethod find-run-column ((board board) column-num)
  (loop for position = `(,column-num 0) then `(,column-num ,(1+ (second position)))
        while (<= (second position) (aref (current-position board) column-num))
        when (find-run board position)
        do (return position)))
 
;;; Just use a modified dijkstra's or something here.
(defmethod remove-run ((board board) position)
  (let ((color (get-position board position)))
    ;; Grabs all adjacent positions that are of the
    ;; same color.
    (flet ((get-same-color (position)
             ;; Grabs all of the valid positions and returns them.
             (let* ((positions (list `(,(first position) ,(1+ (second position)))
                                     `(,(first position) ,(1- (second position)))
                                     `(,(1+ (first position)) ,(second position))
                                     `(,(1- (first position)) ,(second position))))
                    (valid-positions (loop for pos in positions
                                           when (and (<= 0 (first pos))
                                                     (<= 0 (second pos))
                                                     (< (first pos) +width+)
                                                     (< (second pos) +height+))
                                           collect pos))
                    (valid-colors (loop for pos in valid-positions
                                        when (equal color (get-position board pos))
                                        collect pos)))
               valid-colors)))
      ;; Start the stack with the first position.
      (let ((next-position `(,position)))
        (loop while next-position
              do (let* ((next (pop next-position))
                        (valid-positions (get-same-color next)))
                   ;; Put all adjacent spaces of the same color
                   ;; onto the stack.
                   (setf next-position (append valid-positions next-position))
                   ;; Mark each of those spaces as 'visited'.
                   (mapcar #'(lambda (position) (set-position board position nil)) valid-positions))
              ;;condense columns
              finally (loop for i from 0 to (1- +width+)
                            do (condense-column board i)))))))
