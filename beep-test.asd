;;;; beep-test.asd

(in-package #:cl-user)
(defpackage #:beep-test-asd
  (:use #:cl #:asdf #:board))
(in-package #:beep-test-asd)

(defsystem beep-test
  :name "Beep Test"
  :description "Tests the beep program."
  :serial t
  :depends-on (:beep
               :fiveam)
  :components
  ((:module "tests"
            :components
            ((:file "board-test")))))

(load-system 'beep-test)
