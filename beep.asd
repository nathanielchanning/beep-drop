;;;; beep.asd

(in-package #:cl-user)
(defpackage #:beep-asd
  (:use #:cl #:asdf))
(in-package #:beep-asd)

(defsystem beep
  :version "1"
  :description "A Magic Drop type of game."
  :author "Nathaniel Channing <nathaniel@desktopxpressions.com>"
  :components
  ((:module "src"
    :components
    ((:file "board")
     (:file "beep-drop")))))

(load-system 'beep)
