;;;; board-test.lisp

(in-package #:cl-user)

(defpackage #:board-test
  (:use #:cl #:board #:fiveam))

(in-package #:board-test)

(def-suite board-test-suite
    :description "Test that the board package is working as intended.")

(in-suite board-test-suite)

;;;----------ADD-ROW-TESTS-----------
;;; Test that add-row adds a row to the board.
;;; It would be better to check unique numbers
;;; here.
(test add-row-adds-a-row
  (flet ((equality-check (item)
           (equal item 1)))
    (let ((board (make-instance 'board))
          (row '(1 1 1 1 1 1)))
      (add-row board row)
      ;; Grab the first item of every column and check that
      ;; they are equal to 1.
      (is-true (every #'equality-check (loop for i from 0 to (1- +width+)
                                             collect (get-position board `(,i 0))))))))

;;; Check that the each row has 1 item in it now.
;;; Since it is zero-based, we are expecting zeros.
(test add-row-increments-current-position
  (let ((board (make-instance 'board))
        (row '(1 1 1 1 1 1)))
     (add-row board row)
     ;; Grab the first item of every column and check that
     ;; they are equal to 1.
     (is-true (every #'zerop (loop for i from 0 to (1- +width+)
                                   collect (aref (current-position board) i))))))

;;; Check that add row doesn't increment current-position
;;; when nil is being added.
;;; This is purely for testing purposes as of right now.
(test add-row-no-increment-on-nil
  (let ((board (make-instance 'board))
        (row '(nil nil nil nil nil nil))
        (row-2 '(1 nil nil nil nil nil)))
     (add-row board row)
     ;; Grab the first item of every column and check that
     ;; they are equal to 1.
     (is-true (equalp (current-position board) #(-1 -1 -1 -1 -1 -1)))
     (add-row board row-2)
     (is-true (equalp (current-position board) #(0 -1 -1 -1 -1 -1)))))

;;;-------GRAB-COLUMN-TESTS----------
;;; Check that it grabs only the indices
;;; that have the same color.
(test grab-column-gets-same-color
  (let ((board-1 (make-instance 'board))
        (board-2 (make-instance 'board))
        (row-1 '(1 2 3  4  5 6))
        (row-2 '(1 2 3 nil 5 6))
        (row-3 '(7 7 7  7  7 7)))
    (loop for i from 0 to 4
          do (add-row board-1 row-1))
    (loop for i from 0 to 4
          do (add-row board-2 row-2))
    (add-row board-1 row-3)
    (add-row board-2 row-3)
    (grab-column board-1 3)
    ;; This is wrong. Fix this later.
    (is (equalp (columns board-1) (columns board-2)))))

;;; Check that nothing happens when
;;; an empty column is used.
(test grab-column-empty-column
  (let ((board (make-instance 'board))
        (row '(1 2 3 nil 5 6)))
    (loop for i from 0 to 4
          do (add-row board row))
    (is-false (first (grab-column board 3)))))

;;; Check that the return value is
;;; correct.
(test grab-column-return-value
  (let ((board (make-instance 'board))
        (row '(1 2 3 4 5 6)))
    (loop for i from 0 to 4
          do (add-row board row))
    (is-true (equal '(4 4 4 4 4) (grab-column board 3)))))

;;; Check that the current-position
;;; is decremented.
(test grab-column-decrement-position
  (let ((board (make-instance 'board))
        (row '(1 2 3 4 5 6)))
    (loop for i from 0 to 4
          do (add-row board row))
    (grab-column board 3)
    (is (equalp (aref (current-position board) 3) -1))))

;;;----FIND-RUN-DIRECTION-TESTS------
;;; Check that a run is found in every direction.
(test find-run-direction-every-direction
  (let ((board (make-instance 'board))
        (row '(1 1 1 1 5 6)))
    (loop for i from 0 to 4
          do (add-row board row))
    (is-true (equal (find-run-direction board '(0 0) '(1 0))  4) "Failure for the direction Right")
    (is-true (equal (find-run-direction board '(3 0) '(1 0))  4) "Failure for the direction Left")
    (is-true (equal (find-run-direction board '(0 0) '(0 1))  4) "Failure for the direction Down")
    (is-true (equal (find-run-direction board '(0 3) '(0 1))  5) "Failure for the direction Up")
    (is-true (equal (find-run-direction board '(0 1) '(0 1))  5) "Failure for the direction Up and Down")
    (is-true (equal (find-run-direction board '(2 0) '(1 0))  4) "Failure for the direction Left and Right")))

;;; Check that the return value is contiguous.
;;;   A '1 0 0 1' should have a run of 1 not 2.
(test find-run-direction-contiguous
  (let ((board (make-instance 'board))
        (row '(1 1 0 1 5 6)))
    (loop for i from 0 to 4
          do (add-row board row))
    (is-true (equal (find-run-direction board '(0 0) '(1 0)) 2) 
    (is-true (equal (find-run-direction board '(3 0) '(1 0)) 1)))))

;;;--------FIND-RUN-TESTS------------
;;; Check that a valid run is 
;;; returned if it exists.
(test find-run-exists
  (let ((board (make-instance 'board))
        (row '(1 1 0 1 5 6)))
    (loop for i from 0 to 4
          do (add-row board row))
    (add-row board '(1 1 3 1 1 1))
    (is-true  (find-run board '(0 0))) 
    (is-false (find-run board '(2 0)))))

;;;-----FIND-RUN-COLUMN-TESTS--------
(test find-run-column-works
  (let ((board (make-instance 'board))
        (board-2 (make-instance 'board))
        (row '(1 1 0 1 5 6)))
    (loop for i from 0 to 4
          do (add-row board row))
    (is-true (find-run-column board 0) "Failure finding run in column 0.")
    (add-row board-2 '(9 10 11 12 13 14))
    (is-false (find-run-column board-2 0) "Found a run when one doesn't exist.")))

;;;-------REMOVE-RUN-TESTS-----------
;;; Check that the run is removed
;;; and that the current-position
;;; array reflects the changes.
(test remove-run-works
  (let ((board (make-instance 'board))
        (board-2 (make-instance 'board))
        (row '(1 1 0 1 5 6))
        (row-2 '(nil nil 0 nil 5 6)))
    (loop for i from 0 to 4
          do (add-row board row))
    (loop for i from 0 to 4
          do (add-row board-2 row-2))
    (add-row board '(1 1 1 1 1 1))
    (remove-run board '(0 0))
    (is-true (equalp (columns board) (columns board-2)) 
             "Did not return the expected board value.")
    ;; Check that an empty row has a position of -1.
    (is-true (equalp (current-position board) (current-position board-2))
             "Did not return the expected current-position value.")))

;;;------ADD-COLUMN-TESTS------------
;;; Check that the column is added.
(test add-column-works
  (let ((board (make-instance 'board))
        (board-2 (make-instance 'board)))
    (add-column board 0 '(1 1 1 1))
    (loop for i from 0 to 3
          do (add-row board-2 '(1 nil nil nil nil nil)))
    ;; Check that the board is as expected.
    (is-true (equalp (columns board) (columns board-2)))
    ;; Check that the current position is as expected.
    (is-true (equalp (current-position board) #(3 -1 -1 -1 -1 -1)))))

;;; Check that any column overflow is returned.
(test add-column-return-value
  (let ((board (make-instance 'board))
        (column (loop for i from 0 to +height+
                      collect i)))
    (is-true (equal `(,+height+) (add-column board 0 column)))))
    

;;;----------RUN-TESTS---------------
(run! 'board-test-suite)
